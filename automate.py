from datetime import datetime
import subprocess

now = datetime.now()

repeat = 50

for i in range(1,repeat+1):
    current_time = now.strftime("%d-%m-%Y %H:%M:%S")
    with open("README.md", "a") as f:
        f.write(f"<!-- attempt - {i}, time = {current_time} -->\n")
    subprocess.run(['git','add','README.md'])
    subprocess.run(['git','commit','-m',"submit - "+ str(i)])
    subprocess.run(['git','push','origin', 'master'])

